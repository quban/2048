var canvas = document.getElementById('canvas');
var context = canvas.getContext('2d');
var board = [[],[],[],[]];

function drawBoard()
{
  for (var y = 0; y < (4); y++)
  {
    for (var x = 0; x < (4); x++)
    {
      // check what number that should be drawed
      var imgValue = board[y][x];
      var xCoord = (x * 125);
      var yCoord = (y * 125);
      context.fillStyle = "#A8A8A8";
      context.fillRect((xCoord),(yCoord),124,124);
      //draw the numbers
      if(board[y][x] > 0)
      {
        context.fillStyle = "black";
        context.font = "20px Georgia";
        context.fillText(imgValue, (62+(120*x)), (62+(125*y)));
      }
    }
  }
}
// this can be moved into the add function?
function removeZero(array)
{
  // removes all zeros by finding the number and then pushing it into an array
  var newArray = [];
  for (var i = 0; i < 4; i++)
  {
    if (array[i] > 0)
    {
      newArray.push(array[i]);
    }
  }
  return newArray;
}

function addValues(array)
{
  array = removeZero(array);
  if (array.length > 1)
  {
    // checks if 2 values are the same, and if they are they get added
    // by removing the second value and doubling the first value
    for (var x = 0; x < array.length; x++)
    {
      var nextX = x + 1;
      if (array[x] === array[nextX])
      {
        array[x] = array[x]*2;
        array.splice(nextX,1);
      }
    }
  return array;
  }
  else
  {
    // when there is no other value in the array
    return array;
  }
}

function addZeros(direction, array)
{
  //this adds zeros until the width of the array is 4, either in fron or behind
  //depending on what direction you want to move it in
  while(array.length != 4)
  {
    if (direction === 'left' || direction === 'up')
    {
      array.push(0);
    }
    else
    {
      array.unshift(0);
    }
  }
  return array;
}

function emptyCells(gameBoard)
{
  //this finds all the empty cells on the board
  var freeCells = [];
  for (var x = 0; x < 4; x++)
  {
    for (var y = 0; y < 4; y++)
    {
      if (gameBoard[x][y] === 0)
      {
        freeCells.push([x,y]);
      }
    }
  }
  return freeCells;
}

function spawnCell(emptyCells, gameBoard)
{
  // spawns a 2 or a 4 onto the board
  var randomCell = Math.floor(Math.random() * (emptyCells.length));
  var twoOrFour = Math.random() < 0.9 ? 2 : 4;
  var x = emptyCells[randomCell][0];
  var y = emptyCells[randomCell][1];
  gameBoard[x][y] = twoOrFour;
  return gameBoard;
}

function transpose(a)
{
  // This gets all the values in the board
  // and then goes switches the x,y values
  var newA = [[],[],[],[]];
  for (var y = 0; y < 4; y++)
  {
    for (var x = 0; x < 4; x++)
    {
      newA[y][x] = a[x][y];
    }
  }
  return newA;
}

function move(gameBoard, direction)
{
  // moves the board by getting all the x values of a row
  // removing the zeros, adding together and then adding the zeros
  // either in front of the array or in the end.
  // for up and down the board is transposed
  var newBoard = [[],[],[],];
  var xValues = [];
  var transposed = false;
  if (direction === 'up' || direction === 'down')
  {
      gameBoard = transpose(gameBoard);
      transposed = true;
  }
  for (var y = 0;y < 4; y++)
  {
    for (var x = 0; x < 4; x++)
    {
      xValues.push(gameBoard[y][x]);
    }
    var addedCol = [];

    if (direction === 'left' || direction === 'up')
    {
      addedCol = addValues(xValues);
    }
    else // right or down
    {
      xValues.reverse();
      addedCol = addValues(xValues);
      addedCol.reverse();
    }

    var movedCol = addZeros(direction,addedCol);
    // adds the new row the the updated board
    newBoard[y] = movedCol;
    // moves to next row and clears the list
    xValues = [];
  }
  //checking if there have been any changes to the board
  newBoard = boardCheck(gameBoard, newBoard);
  //if the board have been transposed, transpose it back.
  if (transposed)
  {
    newBoard = transpose(newBoard);
  }
  return newBoard;
}

function boardCheck(gameBoard, updatedBoard)
{
   //loops thourgh the boards values to detemine if there have been a change
   //if it have it returns the board with a random spawn
   var c = 0;
   for (var y = 0; y < 4; y++)
   {
     for (var x = 0; x < 4; x++)
     {
       if (gameBoard[y][x] !== updatedBoard[y][x])
       {
         var empty = emptyCells(updatedBoard);
         updatedBoard = spawnCell(empty, updatedBoard);
         return updatedBoard;
       }
       //temporary gameover checker
       if (gameBoard[y][x] > 0)
       {
         c++;
       }
     }
   }
   if (c === 16)
   {
      gameOverCheck(gameBoard);
   }
   return gameBoard;
}
// TODO add random move and see what move/min one can achive
function randomMoves()
{
  // random number in the array and then calls the move funtion, to move it
  // making the move/min
  var moves = ['up','down','right','left'];
  var randomMove = Math.floor((Math.random()*moves.length));
  board = move(board, moves[randomMove]);
}

document.onkeydown = function(e)
{
  // different keys, keycodes for each. Up, down, left, right
  switch (e.keyCode)
  {
    case 38:
    //up
      board = move(board, 'up');
      break;
    case 40:
    //down
      board = move(board, 'down');
      break;
    case 37:
    //left
      board = move(board, 'left');
      break;
    case 39:
    //right
      board = move(board, 'right');
      break;
    case 82:
      randomMoves();
      break;
    default:
      break;
  }
  drawBoard();
};

function gameOverCheck(gameBoard)
{
  // walking through values in the board and checks the number to the right and
  // the number below, if they are the same there is a possible move
  // in the last y list it only checks the right value
  for (var y = 0; y < 4; y++)
  {
    for (var x = 0; x < 4; x++)
    {
      // if not on the last line
      if (y < 3)
      {
        var below = gameBoard[y+1][x];
        var right = gameBoard[y][x+1];
        if (gameBoard[y][x] === right || gameBoard[y][x] === below)
        {
          return gameBoard;
        }
      }
      // not last line, but last x
      else if (x === 3 && y < 3)
      {
        var below = gameBoard[y + 1][x];
        if(gameBoard[y + 1][x] === below)
        {
          return gameBoard;
        }
      }
      // when on last line
      else if(y === 3)
      {
        var right = gameBoard[y][x+1];
        if(gameBoard[y][x] === right)
        {
          return gameBoard;
        }
      }
    }
  }
  // game over, no moves left
  alert("Game Over");
  gameStart(gameBoard);
}
//this draws the start
function gameStart(gameBoard)
{
  //fills the board with 0's
  for (var y = 0; y < 4; y++)
  {
    for (var x = 0; x < 4; x++)
    {
      gameBoard[y][x] = 0;
    }
  }
  // finds all the empty places and spawns a 2 or 4
  var empty = emptyCells(gameBoard);
  gameBoard = spawnCell(empty, gameBoard);
  drawBoard(gameBoard);
}
// starts the game
gameStart(board);
